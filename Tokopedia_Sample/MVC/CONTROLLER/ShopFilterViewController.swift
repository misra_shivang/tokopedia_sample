//
//  ShopFilterViewController.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import UIKit

class ShopFilterViewController: UIViewController {

    //MARK: - IBOutlet
    @IBOutlet weak var tableview: UITableView!
    
    //MARK: - Variables
    var arrayShopFilter : [ShopFilter]?
    var arraySelectedShopFilter = [ShopFilter]()
    var shopFilterAction :  ((_ arraySelectedShopFilter : [ShopFilter])->())?
    
    //MARK: - View delegate
    override func viewDidLoad() {
        
        super.viewDidLoad()
        setup()
    }
}

//MARK: - Setup
extension ShopFilterViewController {
    
    func setup() {
        
        setuptableview()
        setUpArray()
    }
    
    func setUpArray() {
        
        self.arrayShopFilter = [ShopFilter(name: "Gold Mercent", id: "1"), ShopFilter(name: "Official Store", id: "2")]
        self.tableview.reloadData()
    }
    
    func setuptableview() {
        
        tableview.contentInset = Frame.shopFilterContentInset
        if #available(iOS 11.0, *) { tableview.contentInsetAdjustmentBehavior = .never } else { }
        self.tableview.estimatedRowHeight = 0.0
        self.tableview.estimatedSectionHeaderHeight = 0.0
        self.tableview.estimatedSectionFooterHeight = 0.0
        self.tableview.bounces = true
        
        setUpTableviewCell()
        
    }
    
    func setUpTableviewCell() {
        
        let shopFilterCellNib = UINib(nibName: "ShopFilterCell", bundle: nil)
        tableview.register(shopFilterCellNib, forCellReuseIdentifier: CellIdentifier.shopFilterCell.rawValue)
    }
}

//MARK: - IBAction
extension ShopFilterViewController {
    
    @IBAction func btnActionApply(_ sender: Any) {
        self.dismiss(animated: true) {
            self.shopFilterAction?(self.arraySelectedShopFilter)
        }
    }
    
    @IBAction func btnActionCancel(_ sender: Any) {
        
        self.dismissController()
    }
    
    @IBAction func btnActionReset(_ sender: Any) {
        
        if arraySelectedShopFilter.count > 0 {
            self.arraySelectedShopFilter.removeAll()
            self.tableview.reloadData()
        }
    }
}

//MARK: TableView DataSource
extension ShopFilterViewController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        checkNilData(arr: arrayShopFilter, tableView: tableView, contentMode: .center, text : "No information found, please try again in some time")
        
        return (arrayShopFilter?.count)*?
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier:CellIdentifier.shopFilterCell.rawValue) as? ShopFilterCell else { return UITableViewCell() }
        
        let row = indexPath.row
        let shopFilter = self.arrayShopFilter?[row]
        let btnSelectionImage = arraySelectedShopFilter.contains(where: { $0.id == (shopFilter?.id)*? }) ? #imageLiteral(resourceName: "baseline_check_box_black_24pt") : #imageLiteral(resourceName: "baseline_check_box_outline_blank_black_24pt")
        
        cell.tag = row
        cell.lblShopFilter.text = shopFilter?.name
        cell.btnSelection.setImage(btnSelectionImage, for: .normal)
        cell.delegate = self
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return Height.shopFilterCell
    }
}

//MARK: - ProtocolSelection
extension ShopFilterViewController : ProtocolSelection {
    
    func selected(index: Int) {
        
        guard let shopFilter = self.arrayShopFilter?[index] else { return }
        
        if let selectedIndex = arraySelectedShopFilter.index(where: { $0.id == (shopFilter.id)*? }) {
            arraySelectedShopFilter.remove(at: selectedIndex)
        }
        else {
            self.arraySelectedShopFilter.append(shopFilter)
        }
        
        self.tableview.beginUpdates()
        self.tableview.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
        self.tableview.endUpdates()
        
    }
}
