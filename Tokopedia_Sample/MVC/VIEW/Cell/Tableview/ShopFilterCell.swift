//
//  ShopFilterCell.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 13/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import UIKit

protocol ProtocolSelection {
    func selected(index : Int)
}

class ShopFilterCell: UITableViewCell {

    @IBOutlet weak var lblShopFilter: UILabel!
    @IBOutlet weak var btnSelection: UIButton!
    
    var delegate : ProtocolSelection?
    
    override func prepareForReuse() {
        self.btnSelection.setImage(#imageLiteral(resourceName: "baseline_check_box_outline_blank_black_24pt"), for: .normal)
        self.lblShopFilter.text = emptyString
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        initialSetUp(contentBgColor: .clear)
    }
}

extension ShopFilterCell {
    
    @IBAction func btnActionSelection(_ sender: Any) {
        delegate?.selected(index: tag)
    }
}
