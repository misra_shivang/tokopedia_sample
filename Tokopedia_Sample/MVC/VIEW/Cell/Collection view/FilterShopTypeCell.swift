//
//  FilterShopTypeCell.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 15/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import UIKit

protocol ShopTypeCellDelegate {
    func removeAt(indexPath : IndexPath)
}

class FilterShopTypeCell : UICollectionViewCell {
    
    // IBOutlets
    @IBOutlet var lblShopType: UILabel!
    @IBOutlet var viewShopType: UIView!
    @IBOutlet var btnRemoveShopType: UIButton!
    @IBOutlet weak var constraintBtnRemoveWidth: NSLayoutConstraint!
    
    // Variables
    var indexPath : IndexPath?
    var delegate : ShopTypeCellDelegate?
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        // initial setup
        self.viewShopType.layer.cornerRadius = 15.0
        self.viewShopType.layer.masksToBounds = false
        self.viewShopType.layer.borderWidth = 1.0
        self.viewShopType.layer.borderColor = UIColor.lightGray.cgColor
        self.viewShopType.layer.borderColor = UIColor.lightGray.cgColor
        
        self.btnRemoveShopType.layer.borderWidth = 1.0
        self.btnRemoveShopType.layer.cornerRadius = 15.0
        self.btnRemoveShopType.layer.masksToBounds = true
        
        constraintBtnRemoveWidth.constant = self.viewShopType.frame.size.height - 16.0
    }
    
    var shopFilter : ShopFilter? {
        didSet {
            self.lblShopType.text = shopFilter?.name
        }
    }
}

// IBActions
extension FilterShopTypeCell {
    
    @IBAction func removeAction(_ sender: Any) {
        self.delegate?.removeAt(indexPath: self.indexPath*?)
    }
}

