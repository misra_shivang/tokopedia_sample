//
//  SearchCell.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell {
   
    @IBOutlet weak var imageviewProduct: UIImageView!
    @IBOutlet weak var lblPrice: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    
    override func prepareForReuse() {
        
        self.imageviewProduct.image = #imageLiteral(resourceName: "gallery")
        self.lblPrice.text = emptyString
        self.lblProductName.text = emptyString
    }
    
    override func awakeFromNib() {
        
        self.imageviewProduct.layer.cornerRadius = 6.0
    }
    
    var product : ProductDetail? {
        didSet {
            self.lblPrice.text = product?.price
            self.lblProductName.text = product?.name
            ImageCacheManager.shared.download(imageFrom: (product?.image_uri)*?, imageView: imageviewProduct, placHolderimage: #imageLiteral(resourceName: "gallery"), loader: true)
        }
    }
}
