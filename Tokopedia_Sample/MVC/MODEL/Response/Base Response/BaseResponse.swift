//
//  BaseResponse.swift
//  Bar Finder
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

public protocol Unwind {
    
    init(with json : Any?)
}

protocol BaseResponse {
    
    var status : StatusCode? {get set}
    var header : Header? {get set}
}

struct Response : BaseResponse {
    
    var status : StatusCode?
    var header : Header?
}

extension Response : Unwind {
    
    init(with json : Any?) {
        
        status = StatusCode(with : json <-| APIKeys.status.string)
        header = Header(with : json <-| APIKeys.header.string)
    }
}

struct StatusCode {
    
    var error_code : Int?
    var message : String?
    
    init(with json : Any?) {
        
        error_code = json <- APIKeys.error_code.string
        message = json <- APIKeys.message.string
    }
}

struct Header {
    
    var total_data : Int?
    var total_data_no_category : Int?
    var additional_params : String?
    var process_time : Double?
    var suggestion_instead : String?
    
    init(with json : Any?) {
        
        total_data = json <- APIKeys.total_data.string
        total_data_no_category = json <- APIKeys.total_data_no_category.string
        additional_params = json <- APIKeys.additional_params.string
        suggestion_instead = json <- APIKeys.suggestion_instead.string
        process_time = json <- APIKeys.process_time.string
    }
}
