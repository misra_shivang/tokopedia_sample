//
//  EmptyResponse.swift
//  Bar Finder
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

struct EmptyResponse  {
    
    var statusCode : Int?
    var message : String?
    
    init(with result : APIResult) {
        switch result {
        case .failure :
            statusCode = 400
            message = ErrorMessage.somethingWentWrong
        case .internetDown :
            statusCode = 300
            message = ErrorMessage.noInternet
        default : break
        }
    }
}
