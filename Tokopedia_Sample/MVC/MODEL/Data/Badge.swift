//
//  Badge.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

class Badge : NSObject {
    
    var title = String()
    var image_url = String()
    
    init(with json : Any?) {
        
        title = (json <- APIKeys.title.string)*?
        image_url = (json <- APIKeys.image_url.string)*?
    }
    
    static func assign(with arrayBadge : JSONArray?) -> [Badge] {
        
        guard let _arrayBadge = arrayBadge else { return [] }
        let array = _arrayBadge.map { Badge(with: $0) }
        return array
    }
}
