//
//  ShopFilter.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 15/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

class ShopFilter {
    
    var name : String?
    var id : String?
    
    init(name : String?, id : String?) {
         self.name = name
        self.id = id
    }
}
