//
//  ShopProduct.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

class ShopProduct  : NSObject {
    
    var id = Int()
    var name = String()
    var uri = String()
    var is_gold = Int()
    var rating = Int()
    var location = String()
    var reputation_image_uri = String()
    var shop_lucky = String()
    var city = String()
    
    init(with json : Any?) {
        
        id = (json <- APIKeys.id.string)*?
        name = (json <- APIKeys.name.string)*?
        uri = (json <- APIKeys.uri.string)*?
        is_gold = (json <- APIKeys.is_gold.string)*?
        rating = (json <- APIKeys.rating.string)*?
        location = (json <- APIKeys.location.string)*?
        reputation_image_uri = (json <- APIKeys.reputation_image_uri.string)*?
        shop_lucky = (json <- APIKeys.shop_lucky.string)*?
        city = (json <- APIKeys.city.string)*?
    }
}
