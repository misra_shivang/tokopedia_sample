//
//  WholesalePrice.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

class WholesalePrice {
    
    var count_min = Int()
    var count_max = Int()
    var price = String()
    
    init(with json : Any?) {
        
        count_min = (json <- APIKeys.count_min.string)*?
        count_max = (json <- APIKeys.count_max.string)*?
        price = (json <- APIKeys.price.string)*?
    }
    
    static func assign(with arrayWholesalePrice : JSONArray?) -> [WholesalePrice] {
        
        guard let _arrayWholesalePrice = arrayWholesalePrice else { return [] }
        let array = _arrayWholesalePrice.map { WholesalePrice(with: $0) }
        return array
    }
}
