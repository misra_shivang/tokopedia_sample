//
//  Filter.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 15/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit

struct Filter {
    
    var maxPrice : CGFloat?
    var minPrice : CGFloat?
    var wholeSale = false
    var arrayShopType : [ShopFilter]?
}
