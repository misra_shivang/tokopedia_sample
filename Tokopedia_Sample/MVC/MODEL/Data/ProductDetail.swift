//
//  ProductDetail.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

class ProductDetail : NSObject {
    
    var id = Int()
    var name = String()
    var uri = String()
    var image_uri = String()
    var image_uri_700 = String()
    var price = String()
    var price_range = String()
    var category_breadcrumb = String()
    var shop : ShopProduct?
    var condition = Int()
    var preorder = Int()
    var department_id = Int()
    var rating = Int()
    var is_featured = Int()
    var count_review = Int()
    var count_talk = Int()
    var count_sold = Int()
    var original_price = String()
    var discount_expired = String()
    var discount_start = Int()
    var discount_percentage = Int()
    var stock = Int()
    
    var wholesale_price : [WholesalePrice]?
    var labels : [Label]?
    var badges : [Badge]?
    
    init(with json : Any?) {
        
        id = (json <- APIKeys.id.string)*?
        name = (json <- APIKeys.name.string)*?
        uri = (json <- APIKeys.uri.string)*?
        image_uri = (json <- APIKeys.image_uri.string)*?
        image_uri_700 = (json <- APIKeys.image_uri_700.string)*?
        price = (json <- APIKeys.price.string)*?
        price_range = (json <- APIKeys.price_range.string)*?
        category_breadcrumb = (json <- APIKeys.category_breadcrumb.string)*?
        shop = ShopProduct(with: json <-| APIKeys.shop.string)
        condition = (json <- APIKeys.condition.string)*?
        preorder = (json <- APIKeys.preorder.string)*?
        department_id = (json <- APIKeys.department_id.string)*?
        rating = (json <- APIKeys.rating.string)*?
        is_featured = (json <- APIKeys.is_featured.string)*?
        count_review = (json <- APIKeys.count_review.string)*?
        count_talk = (json <- APIKeys.count_talk.string)*?
        count_sold = (json <- APIKeys.count_sold.string)*?
        original_price = (json <- APIKeys.original_price.string)*?
        discount_expired = (json <- APIKeys.discount_expired.string)*?
        discount_start = (json <- APIKeys.discount_start.string)*?
        discount_percentage = (json <- APIKeys.discount_percentage.string)*?
        stock = (json <- APIKeys.stock.string)*?
        
        wholesale_price = WholesalePrice.assign(with: json <= APIKeys.wholesale_price.string)
        labels = Label.assign(with: json <= APIKeys.labels.string)
        badges = Badge.assign(with: json <= APIKeys.badges.string)
    }
}

extension ProductDetail {
    
    static func assign(with productDetailList : JSONArray?) -> [ProductDetail]? {
        
        guard let _productDetailList = productDetailList else { return nil }
        let array = _productDetailList.map { ProductDetail(with: $0) }
        return array
    }
}
