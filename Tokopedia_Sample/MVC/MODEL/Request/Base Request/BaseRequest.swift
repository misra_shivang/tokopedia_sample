//
//  BaseRequest.swift
//  Bar Finder
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import Alamofire

protocol JSONAble {}

extension JSONAble {
    
    func toDict() -> [String : Any] {
        var dict = [String : Any]()
        let otherSelf = Mirror(reflecting: self)
        for child in otherSelf.children {
            if let key = child.label {
                dict[key] = child.value
            }
        }
        return dict
    }
}

public class BaseRequest : NSObject {

    var url : APIResourcePath?
    var method : APIMethod?
    
    init(with Url : APIResourcePath, Method : APIMethod = .post) {
        self.url = Url
        self.method = Method
    }
}

class Request : BaseRequest,JSONAble { }
