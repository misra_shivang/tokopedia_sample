//
//  LocalStore.swift
//  Tokopedia_Sample
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation


class LocalStore {
    
    //MARK: - Keys
    private enum Keys : CustomStringConvertible {
        
        case accessToken
        
        var description : String {
            
            switch self {
                case .accessToken : return "ACCESSTOKEN_Delta"
            }
        }
    }
    
    static let shared : LocalStore = {
        let instance = LocalStore()
        return instance
    }()
    
    private static let userDefaults = UserDefaults.standard
    
    //MARK: - Setter Getter Delete of Device Token
    static var accessToken : String? {
        get {
            return userDefaults.object(forKey: Keys.accessToken.description) as? String
        }
        set{
            guard let value = newValue else {
                userDefaults.removeObject(forKey: Keys.accessToken.description)
                return
            }
            userDefaults.set(value, forKey: Keys.accessToken.description)
            userDefaults.synchronize()
        }
    }
}
