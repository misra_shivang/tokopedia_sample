//
//  CustomNavBar.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//


import UIKit
import Foundation


class CustomNavBar: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    func prepareView() {
        
        if let constraint = (self.constraints.filter{$0.firstAttribute == .height}.first) {
            constraint.constant = UIDevice.current.modelName.isEqual("iPhone X") ? 84.0 : 64.0
        }
        
    }
    
}

class CustomXBtn: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        prepareView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        prepareView()
    }
    
    func prepareView() {
        
        if let constraint = (self.constraints.filter{$0.firstAttribute == .height}.first) {
            constraint.constant = UIDevice.current.modelName.isEqual("iPhone X") ? constraint.constant + 16.0 : constraint.constant
        }
        
    }
    
}

