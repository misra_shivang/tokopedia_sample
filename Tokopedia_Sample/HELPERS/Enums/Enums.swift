//
//  Enums.swift
//  Tokopedia_Sample
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


//MARK: - API Status Code
public enum APIStatusCode : Int {
    
    case success = 200
    case failure = 400
    case server = 500
}

//MARK: - DataType
public enum EnumDataType : String {
    
    case json = "json"
    case xml = "xml"
}

//MARK: - Date Format
public enum EnumDateFormat : String {
    
    case MMMddyyyyHHmmA = "MMM dd, yyyy · hh:mm a"
    case MMMddyyyy = "MMM dd, yyyy"
    case yyyyMMddHHmmss = "yyyy-MM-dd HH:mm:ss"
    case utc = "yyyy-MM-dd'T'HH:mm:ssZ"
    case hhmmss = "HH:mm:ss"
    case hhmmssA = "HH:mm:ss a"
    case yyyyMMdd = "yyyy-MM-dd"
    case mmddyyyy = "MM/dd/yyyy"
    case hhmm = "HH:mm"
    case hhmmA = "HH:mm a"
    case hh = "HH"
    case ee = "EE"
    case eeee = "EEEE"
    case hhA = "hh a"
    case hA = "h a"
    case h_mmA = "h:mm a"
    case hh_mmA = "hh:mm a"
    case mmmmdd = "MMMM dd"
    case MMMdd = "MMM dd"
    case MMMdd_hh_mmA = "MMM dd h:mm a"
    case EEE_HH_MM_A = "EEE h:mm a"
    case chatDate = "dd EEEE yyyy"
    case dayMonth = "EE MMM dd"
    case mmm = "MMM"
    case dd = "dd"
}

