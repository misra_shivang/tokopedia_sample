//
//  SwiftMessageAlert.swift
//  Tokopedia_Sample
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit

enum SwiftAlertType: Int {
    case error
    case info
    case success
    case otp
}

enum EnumSwiftMessageTitleAlert : String {
    
    case success = "Success"
    case oops = "Oops"
    case login = "Login Successfull"
    case ok = "Ok"
    case cancel = "Cancel"
    case error = "Error"
    case warning = "Warning"
    case failedFbLogin = "Facebook login failed"
    case otp = "OTP"
}

