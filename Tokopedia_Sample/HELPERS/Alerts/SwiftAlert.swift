
//  Tokopedia_Sample
//
//  Created by Shivang Mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//
import Foundation
import UIKit

class Alert {
    
    var okAction : AlertSuccess?
    typealias AlertSuccess = (()->())?
    
    public func show(title : String?, message : String?, viewController : UIViewController?, okAction : AlertSuccess = nil) {
        
        let version : NSString = UIDevice.current.systemVersion as NSString
        if  version.doubleValue >= 8 {
            let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle:.alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action: UIAlertAction) in
                
                if let okAction = okAction {
                    okAction()
                }
            }))
            viewController?.present(alert, animated:true, completion:nil);
        }
    }

    public func showWithCancelAndOk(title : String?, okTitle : String, cancelTitle : String, message : String, viewController : UIViewController?, okAction : AlertSuccess = nil, cancelAction : AlertSuccess = nil) {
        let version:NSString = UIDevice.current.systemVersion as NSString;
        
        if  version.doubleValue >= 8 {
            let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle:.alert)
            
            alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: { (action: UIAlertAction) in
                
                if let cancelAction = cancelAction {
                    cancelAction()
                }
            }))
            alert.addAction(UIAlertAction(title: okTitle, style: .default, handler: { (action: UIAlertAction) in
                
                if let okAction = okAction {
                    okAction()
                }
            }))
            viewController?.present(alert, animated:true, completion:nil);
        }
    }
}
