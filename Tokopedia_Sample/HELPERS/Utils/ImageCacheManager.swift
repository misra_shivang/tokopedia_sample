//
//  ImageCacheManager.swift
//  Tokopedia_Sample
//
//  Created by Shivang Mishra on 01/02/17.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class ImageCacheManager {
    
    static let shared : ImageCacheManager = {
        let instance = ImageCacheManager()
        return instance
    }()
    
    func download(imageFrom imageURL:String ,imageView : UIImageView, placHolderimage : UIImage? = nil, loader:Bool? = true)  {
        
        if loader*? {
            imageView.kf.indicatorType = .activity
        }
        
        guard let url = URL(string: imageURL) else { return }
        imageView.kf.setImage(with: url,
                              placeholder : placHolderimage,
                              options: [.transition(.fade(1))],
                              progressBlock: nil,
                              completionHandler: nil)
    }
}

extension UIImage {
    
    func saveToGallery() -> String {
        
        let documentsDirectoryURL = try! FileManager().url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let fileURL = documentsDirectoryURL.appendingPathComponent("Image"+"\(arc4random())"+".jpeg")
        do {
            let data = UIImageJPEGRepresentation(self, 0.5)
            try data?.write(to: fileURL)
            return fileURL.path
            // try FileManager.default.copyItem(at: URL(fileURLWithPath: editedVideoPath), to: fileURL)
        }catch {
            return "error"
        }
    }

}
