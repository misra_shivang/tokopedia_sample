//
//  Storyboards.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit


protocol StoryboardSceneType {
    static var storyboardName: String { get }
}

extension StoryboardSceneType {
    static func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.storyboardName, bundle: Bundle(for: BundleToken.self))
    }
    
    static func initialViewController() -> UIViewController {
        guard let vc = storyboard().instantiateInitialViewController() else {
            fatalError("Failed to instantiate initialViewController for \(self.storyboardName)")
        }
        return vc
    }
}

extension StoryboardSceneType where Self: RawRepresentable, Self.RawValue == String {
    func viewController() -> UIViewController {
        return Self.storyboard().instantiateViewController(withIdentifier: self.rawValue)
    }
    static func viewController(identifier: Self) -> UIViewController {
        return identifier.viewController()
    }
}

protocol StoryboardSegueType: RawRepresentable { }

extension UIViewController {
    func perform<S: StoryboardSegueType>(segue: S, sender: Any? = nil) where S.RawValue == String {
        performSegue(withIdentifier: segue.rawValue, sender: sender)
    }
}

enum StoryboardScene {
    enum Main: String, StoryboardSceneType {
        static let storyboardName = "Main"
        
        case searchViewControllerScene = "SearchViewController"
        static func instantiateSearchViewControllerScene() -> SearchViewController {
            guard let vc = StoryboardScene.Main.searchViewControllerScene.viewController() as? SearchViewController
                else {
                    fatalError("ViewController 'SearchViewController' is not of the expected class UDown.CustomEventsViewController.")
            }
            return vc
        }
        
        case filterViewControllerScene = "FilterViewController"
        static func instantiateFilterViewController() -> FilterViewController {
            guard let vc = StoryboardScene.Main.filterViewControllerScene.viewController() as? FilterViewController
                else {
                    fatalError("ViewController 'FilterViewController' is not of the expected class UDown.CustomEventsViewController.")
            }
            return vc
        }
        
        case shopFilterViewControllerScene = "ShopFilterViewController"
        static func instantiateShopFilterViewController() -> ShopFilterViewController {
            guard let vc = StoryboardScene.Main.shopFilterViewControllerScene.viewController() as? ShopFilterViewController
                else {
                    fatalError("ViewController 'ShopFilterViewController' is not of the expected class UDown.CustomEventsViewController.")
            }
            return vc
        }
    }
}


enum StoryboardSegue {
}

private final class BundleToken {}
