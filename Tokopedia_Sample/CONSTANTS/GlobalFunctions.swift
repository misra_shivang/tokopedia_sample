//
//  GlobalFunctions.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit

func checkNilData(arr: Array<AnyObject>?, tableView: UITableView?, contentMode: UIViewContentMode, text : String) {
    
    switch (arr?.count)*? > 0 {
    case true:
        tableView?.backgroundView = nil
    default:
        if tableView?.backgroundView == nil {
            let noDataView = UILabel(frame: CGRect(x:0,y: 0,width: (tableView?.bounds.size.width)*?,height: (tableView?.bounds.size.height)*?))
            noDataView.contentMode = contentMode
            noDataView.font = GlobalConstants.Font.avenirHeavy14
            noDataView.textAlignment = .center
            noDataView.textColor = GlobalConstants.Color.grey
            noDataView.text = text
            noDataView.isHidden = false
            noDataView.alpha = 0.0
            tableView?.backgroundView = noDataView
            UIView.animate(withDuration: 2.0, delay: 0, options: .curveEaseInOut, animations: {
                noDataView.alpha = 1.0
            }, completion: { _ in })
        }
    }
}

func checkNilData(arr: Array<AnyObject>?, collectionView: UICollectionView?, contentMode: UIViewContentMode, text: String) {
    
    switch (arr?.count)*? > 0 {
    case true:
        collectionView?.backgroundView = nil
    default:
        if collectionView?.backgroundView == nil {
            let noDataView = UILabel(frame: CGRect(x:0,y: 0,width: (collectionView?.bounds.size.width)*?,height: (collectionView?.bounds.size.height)*?))
            noDataView.contentMode = contentMode
            noDataView.font = GlobalConstants.Font.avenirHeavy14
            noDataView.textAlignment = .center
            noDataView.textColor = GlobalConstants.Color.grey
            noDataView.text = text
            noDataView.isHidden = false
            noDataView.alpha = 0.0
            collectionView?.backgroundView = noDataView
            UIView.animate(withDuration: 2.0, delay: 0, options: .curveEaseInOut, animations: {
                noDataView.alpha = 1.0
            }, completion: { _ in })
        }
    }
}
