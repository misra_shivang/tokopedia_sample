//
//  HeightFrames.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation
import UIKit

//MARK: - Height
struct Height {
    
    static let shopFilterCell : CGFloat = 60.0
    static let filterCollectionview : CGFloat = 72.0
}

struct Frame {
    
    static let shopFilterContentInset = UIEdgeInsets(top: 8.0, left: 0.0, bottom: 0.0, right: 0.0)
}
