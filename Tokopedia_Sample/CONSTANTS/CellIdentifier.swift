//
//  CellIdentifier.swift
//  Tokopedia_Sample
//
//  Created by Shivang mishra on 14/05/18.
//  Copyright © 2018 Shivang mishra. All rights reserved.
//

import Foundation

//MARK: - Cell Identifier

enum CellIdentifier : String {
    
    case searchCell = "SearchCell"
    case shopFilterCell = "ShopFilterCell"
    case filterShopTypeCell = "FilterShopTypeCell"
}
